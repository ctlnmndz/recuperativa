#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

# Función para extraer json.
def load_file():

    with open('iris.json', 'r') as file:
        iris = json.load(file)
        
    return iris


# Función para sacar el mayor valor del alto de las sepa
def alto_max():

    mayor = 0
    # Recorre el diccionario 
    for i in iris: 
        for key, value in i.items():
            # Se evaluan el largo de las sepas
            if key == "sepalLength":
                if value > mayor:
                    mayor = value
                    especie = i["species"]

    texto = "La medida máxima para un alto es {} ".format(mayor)
    texto = texto + " y lo tiene la especie {}.".format(especie)
    print(texto)
    

# Función para buscar los nombres de las especies
def nombre_especies():

    especies = []
    # Recorre el diccionario
    for i in iris:
        for key, value in i.items():
            # Se evaluan las especies
            if key == "species":
                # Si la especie esta en la lista no hace nada
                if value in especies:
                    pass
                # Sino se guarda en la lista
                else:
                    especies.append(value)
    
    print("Las especies son:")
    for nombres in especies:
        print "- ", nombres


# Función para sacar que especie tiene el mayo promedio de alto
def mayor_alto(promedio_alto_setosa, promedio_alto_versicolor, promedio_alto_virginica):
    
    promedios_altos = []
    # Los promedios se añaden a la lista
    promedios_altos.append(promedio_alto_setosa)
    promedios_altos.append(promedio_alto_versicolor)
    promedios_altos.append(promedio_alto_virginica)
    
    # Se ordena la lista de menor a mayor
    promedios_altos.sort()
    
    # Se evalua el mayor promedio con los promedios para ver de que especie es
    if promedios_altos[2] == promedio_alto_setosa:
	    print("El promedio mayor del promedio alto lo tiene la especie la setosa")
    elif promedios_altos[2] == promedio_alto_versicolor:
	    print("El promedio mayor del promedio alto lo tiene la especie versicolor")
    else:
	    print("El promedio mayor del promedio alto lo tiene la especie virginica")


# Función para sacar que especie tiene el mayo promedio de ancho
def mayor_ancho(promedio_ancho_setosa, promedio_ancho_versicolor, promedio_ancho_virginica):
    
    promedios_anchos = []
    # Los promedios se añaden a la lista
    promedios_anchos.append(promedio_ancho_setosa)
    promedios_anchos.append(promedio_ancho_versicolor)
    promedios_anchos.append(promedio_ancho_virginica)
    
    # Se ordena la lista de menor a mayor
    promedios_anchos.sort()
    
    # Se evalua el mayor promedio con los promedios para ver de que especie es
    if promedios_anchos[2] == promedio_ancho_setosa:
	    print("El promedio mayor del promedio ancho lo tiene la especie setosa")
    elif promedios_anchos[2] == promedio_ancho_versicolor:
	    print("El promedio mayor del promedio ancho lo tiene la especie versicolor")
    else:
	    print("El promedio mayor del promedio ancho lo tiene la especie virginica")


# Función para sacar los promdeios    
def promedio_alto_ancho():
    
    # Variables a utilizar
    suma_largo_setosa = 0
    suma_ancho_setosa = 0
    suma_largo_versicolor = 0
    suma_ancho_versicolor = 0
    suma_largo_virginica = 0
    suma_ancho_virginica = 0
    contador_setosa = 0
    contador_versicolor = 0
    contador_virginica = 0 

    # Se recorre el diccionario
    for i in iris:
        # Saca la suma y la cantidad de las setosas
        if i["species"] == "setosa":
            contador_setosa = contador_setosa + 1
            largo_setosa = i["petalLength"]
            suma_largo_setosa = largo_setosa + suma_largo_setosa
            ancho_setosa = i["petalWidth"]
            suma_ancho_setosa = ancho_setosa + suma_ancho_setosa
      
        # Saca la suma y la cantidad de las versicolor
        if i["species"] == "versicolor":
            contador_versicolor = contador_versicolor + 1 
            largo_versicolor = i["petalLength"]
            suma_largo_versicolor = largo_versicolor + suma_largo_versicolor
            ancho_versicolor = i["petalWidth"]
            suma_ancho_versicolor = ancho_versicolor + suma_ancho_versicolor
		
        # Saca la suma y la cantidad de las virginicas
        if i["species"] == "virginica":
            contador_virginica = contador_virginica + 1 
            largo_virginica = i["petalLength"]
            suma_largo_virginica = largo_virginica + suma_largo_virginica
            ancho_virginica= i["petalWidth"]
            suma_ancho_versicolor = ancho_virginica + suma_ancho_virginica
    
    # Se divide las sumas con la cantidad de especies para sacar promedio
    promedio_alto_setosa = suma_largo_setosa/contador_setosa
    promedio_ancho_setosa = suma_ancho_setosa/contador_setosa
    promedio_alto_versicolor = suma_largo_versicolor/contador_versicolor
    promedio_ancho_versicolor = suma_ancho_versicolor/contador_versicolor
    promedio_alto_virginica = suma_largo_virginica/contador_virginica
    promedio_ancho_virginica = suma_largo_virginica/contador_virginica  
	
    print("El promedio de la altura de la setosa es: ", promedio_alto_setosa)
    print("El promedio del ancho de la setosa es: ", promedio_ancho_setosa)
    print("El promedio de la altura de la versicolor es: ", promedio_alto_versicolor)
    print("El promedio del ancho de la versicolor es: ", promedio_ancho_versicolor)
    print("El promedio de la altura de la virginica es: ",  promedio_alto_virginica)
    print("El promedio del ancho de la virginica es: ", promedio_ancho_virginica)
    
    # Se llaman funciones para ver que especie tiene el mayor promedio
    mayor_alto(promedio_alto_setosa, promedio_alto_versicolor, promedio_alto_virginica)
    mayor_ancho(promedio_ancho_setosa, promedio_ancho_versicolor, promedio_ancho_virginica)
  

# Main
if __name__ == '__main__':
    
    # Se llaman funciones 
    iris = load_file()
    
    nombre_especies()
    promedio_alto_ancho()
    alto_max()
    
